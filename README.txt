## How to run:

If you have Maven installed just run "mvn spring-boot:run"
If no project contains mvnw (https://github.com/takari/maven-wrapper)

Project will start on localhost:8080

http://localhost:8080/round?bet=ROCK will run one game round
Possible bets: ROCK, SCISSORS, PAPER

http://localhost:8080/history will return history


## Main concepts
Main concept is to have possibility to use a lot of different prediction strategies (BetPredictionStrategy.java)
Which will do independent predictions based on history or not ot even just random bet

For demo puprpose only few strategies was implemented:
- Random strategy
- Previous bot choice
- Most frequent players choice (for demo puprpose only this strategy is taken into account. See BetNegotiation.java)

Additional strategies could be easy added.
It's enough to implement BetPredictionStrategy interface and mark it as @Component
Some possible strategies:
- Find in history same round as last and see next
- Find in history n players choices and see next
- Create own static patter not based on history

Main idea was to create ease to extent skeleton.

## Tech stack
Spring Boot/Rest, JUnit, Mockito, Maven