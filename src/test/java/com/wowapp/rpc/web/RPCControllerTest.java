package com.wowapp.rpc.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wowapp.rpc.domain.Bet;
import com.wowapp.rpc.domain.HistoryHolder;
import com.wowapp.rpc.domain.RPCService;
import com.wowapp.rpc.domain.RoundResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static java.util.Arrays.asList;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Author Sergii Motynga.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(RPCController.class)
public class RPCControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private RPCService service;

    @MockBean
    private HistoryHolder historyHolder;


    @Test
    public void shouldProcessRound() throws Exception {
        //given
        Bet playersBet = Bet.ROCK;
        RoundResult roundResult = RoundResult.of(playersBet, Bet.PAPER);

        given(service.process(playersBet)).willReturn(roundResult);

        //then
        mvc.perform(get("/round").param("bet", playersBet.name()))
                .andExpect(status().isOk())
                .andExpect(content().string(mapper.writeValueAsString(roundResult)));
    }

    @Test
    public void shouldReturnHistory() throws Exception {
        //given
        List<RoundResult> history = asList(RoundResult.of(Bet.ROCK, Bet.ROCK));

        given(historyHolder.getAllHistory()).willReturn(history);

        //then
        mvc.perform(get("/history"))
                .andExpect(status().isOk())
                .andExpect(content().string(mapper.writeValueAsString(history)));

    }
}
