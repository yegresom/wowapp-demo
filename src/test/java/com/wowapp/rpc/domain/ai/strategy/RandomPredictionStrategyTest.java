package com.wowapp.rpc.domain.ai.strategy;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Author Sergii Motynga.
 */
public class RandomPredictionStrategyTest {

    @Test
    public void shouldReturnRandomBet() {
        assertThat(new RandomPredictionStrategy().prediction().isPresent()).isTrue();
    }
}
