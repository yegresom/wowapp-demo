package com.wowapp.rpc.domain.ai;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.wowapp.rpc.domain.Bet.*;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentCaptor.forClass;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

/**
 * Author Sergii Motynga.
 */
@RunWith(MockitoJUnitRunner.class)
public class PredictionServiceTest {

    @Mock
    private BetNegotiation negotiation;

    @Mock
    private BetPredictionStrategy strategy;

    @Captor
    private ArgumentCaptor<List<Prediction>> captor;

    @Test
    public void shouldCollectAllPredictionsAndAskChooserForResult() {
        //given
        PredictionService predictionService =
                new PredictionService(asList(strategy, strategy, strategy, strategy), negotiation);
        given(strategy.prediction())
                .willReturn(Optional.of(ROCK))
                .willReturn(Optional.of(PAPER))
                .willReturn(Optional.empty())
                .willReturn(Optional.of(SCISSORS));

        //when
        predictionService.makeBet();

        //then
        verify(negotiation).makeChoose(captor.capture());
        assertThat(
                captor.getValue().stream().map(Prediction::getBet).map(Optional::get).collect(toList())
        ).contains(ROCK, PAPER, SCISSORS);
    }

    @Test
    public void shouldReturnBestBet() {
        //given
        PredictionService predictionService = new PredictionService(emptyList(), negotiation);
        given(negotiation.makeChoose(anyList())).willReturn(ROCK);

        //then
        assertThat(predictionService.makeBet()).isEqualTo(ROCK);
    }

}
