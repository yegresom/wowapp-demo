package com.wowapp.rpc.domain.ai;

import com.wowapp.rpc.domain.Bet;
import com.wowapp.rpc.domain.ai.strategy.MostFrequentPlayersBetPredictionStrategy;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.emptyList;

/**
 * Author Sergii Motynga.
 */
public class BetNegotiationTest {

    @Test
    public void shouldReturnBetOf_MostFrequentPlayersBetPredictionStrategy() {
        Bet expected = Bet.ROCK;
        List<Prediction> predictions = Arrays.asList(
                Prediction.of(Optional.of(expected), MostFrequentPlayersBetPredictionStrategy.class),
                Prediction.of(Optional.of(Bet.PAPER), BetPredictionStrategy.class),
                Prediction.of(Optional.of(Bet.SCISSORS), BetPredictionStrategy.class)
        );

        //when
        Bet result = new BetNegotiation().makeChoose(predictions);

        //then
        Assertions.assertThat(result).isEqualTo(expected);
    }
}
