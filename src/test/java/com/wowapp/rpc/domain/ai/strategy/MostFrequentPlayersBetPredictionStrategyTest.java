package com.wowapp.rpc.domain.ai.strategy;

import com.wowapp.rpc.domain.HistoryHolder;
import com.wowapp.rpc.domain.RoundResult;
import com.wowapp.rpc.domain.ai.strategy.MostFrequentPlayersBetPredictionStrategy;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.wowapp.rpc.domain.Bet.*;
import static com.wowapp.rpc.domain.Bet.ROCK;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

/**
 * Author Sergii Motynga.
 */
@RunWith(MockitoJUnitRunner.class)
public class MostFrequentPlayersBetPredictionStrategyTest {

    @InjectMocks
    private MostFrequentPlayersBetPredictionStrategy strategy;

    @Mock
    private HistoryHolder historyHolder;

    @Test
    public void shouldReturnEmptyIf_NoHistory() {
        assertThat(strategy.prediction()).isEqualTo(Optional.empty());
    }

    @Test
    public void shouldReturnAntagonistForMostFrequentPlayersChoice() {
        List<RoundResult> history = Arrays.asList(
                RoundResult.of(ROCK, SCISSORS),
                RoundResult.of(PAPER, SCISSORS),
                RoundResult.of(ROCK, ROCK),
                RoundResult.of(ROCK, PAPER),
                RoundResult.of(SCISSORS, SCISSORS));

        given(historyHolder.getAllHistory()).willReturn(history);

        assertThat(strategy.prediction()).isEqualTo(Optional.of(antagonist(ROCK)));
    }
}
