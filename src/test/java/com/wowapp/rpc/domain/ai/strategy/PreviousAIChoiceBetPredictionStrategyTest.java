package com.wowapp.rpc.domain.ai.strategy;

import com.wowapp.rpc.domain.Bet;
import com.wowapp.rpc.domain.HistoryHolder;
import com.wowapp.rpc.domain.RoundResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

/**
 * Author Sergii Motynga.
 */
@RunWith(MockitoJUnitRunner.class)
public class PreviousAIChoiceBetPredictionStrategyTest {

    @InjectMocks
    private PreviousAIChoiceBetPredictionStrategy strategy;

    @Mock
    private HistoryHolder history;

    @Test
    public void shouldReturnEmptyIf_NoHistory() {
        assertThat(strategy.prediction()).isEqualTo(Optional.empty());
    }

    @Test
    public void shouldReturnAntagonistForLastAIBet() {
        //given
        RoundResult lastRound = RoundResult.of(Bet.SCISSORS, Bet.ROCK);
        given(history.getAllHistory()).willReturn(singletonList(lastRound));

        //when
        Bet result = strategy.prediction().get();

        //then
        assertThat(result).isEqualTo(Bet.antagonist(lastRound.getAiBet()));
    }
}
