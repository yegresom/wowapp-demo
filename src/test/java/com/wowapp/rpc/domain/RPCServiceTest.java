package com.wowapp.rpc.domain;

import com.wowapp.rpc.domain.ai.PredictionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.wowapp.rpc.domain.Bet.PAPER;
import static com.wowapp.rpc.domain.Bet.ROCK;
import static com.wowapp.rpc.domain.Bet.SCISSORS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentCaptor.forClass;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

/**
 * Author Sergii Motynga.
 */
@RunWith(MockitoJUnitRunner.class)
public class RPCServiceTest {

    @InjectMocks
    private RPCService service;

    @Mock
    private PredictionService predictionService;

    @Mock
    private HistoryHolder historyHolder;

    @Test
    public void shouldReturnRoundResult() {
        //given
        given(predictionService.makeBet()).willReturn(ROCK);

        //when
        RoundResult result = service.process(SCISSORS);

        //then
        assertThat(result).isNotNull();
        assertThat(result.getAiBet()).isEqualTo(ROCK);
        assertThat(result.getPlayersBet()).isEqualTo(SCISSORS);
    }

    @Test
    public void shouldSaveHistory() {
        //given
        given(predictionService.makeBet()).willReturn(ROCK);
        ArgumentCaptor<RoundResult> captor = forClass(RoundResult.class);

        //when
        RoundResult result = service.process(PAPER);

        //then
        verify(historyHolder).add(captor.capture());
        assertThat(captor.getValue()).isEqualTo(result);
    }
}
