package com.wowapp.rpc.domain;

import org.junit.Test;

import static com.wowapp.rpc.domain.Bet.*;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Author Sergii Motynga.
 */
public class BetTest {

    @Test
    public void shouldReturnCorrectAntagonists() {
        assertThat(antagonist(PAPER)).isEqualTo(SCISSORS);
        assertThat(antagonist(SCISSORS)).isEqualTo(ROCK);
        assertThat(antagonist(ROCK)).isEqualTo(PAPER);
    }
}
