package com.wowapp.rpc.domain;

import com.wowapp.rpc.domain.ai.PredictionService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;


/**
 * Author Sergii Motynga.
 */
@Service
@AllArgsConstructor
public class RPCService {

    private PredictionService predictionService;

    private HistoryHolder historyHolder;

    public RoundResult process(Bet playersBet) {
        Bet aiBet = predictionService.makeBet();
        RoundResult roundResult = RoundResult.of(playersBet, aiBet);
        historyHolder.add(roundResult);
        return roundResult;
    }
}
