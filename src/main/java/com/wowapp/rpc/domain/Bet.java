package com.wowapp.rpc.domain;

/**
 * Author Sergii Motynga.
 */
public enum Bet {
    ROCK,
    SCISSORS,
    PAPER;

    public static Bet antagonist(Bet bet) {
        if (PAPER.equals(bet)) return SCISSORS;
        if (SCISSORS.equals(bet)) return ROCK;
        else return PAPER;
    }
}
