package com.wowapp.rpc.domain;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Author Sergii Motynga.
 */
@Component
public class HistoryHolder {

    //in real concurrent project I would use more sophisticated collection for personalized history
    private List<RoundResult> history = new ArrayList<>();

    public void add(RoundResult roundResult) {
        history.add(roundResult);
    }

    public List<RoundResult> getAllHistory() {
        return history;
    }

}
