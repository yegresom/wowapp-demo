package com.wowapp.rpc.domain.ai;

import com.wowapp.rpc.domain.Bet;

import java.util.Optional;

/**
 * Author Sergii Motynga.
 */
public interface BetPredictionStrategy {
    /**
     * @return bets bet based on prediction strategy
     */
    Optional<Bet> prediction();
}
