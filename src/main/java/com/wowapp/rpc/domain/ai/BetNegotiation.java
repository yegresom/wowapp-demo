package com.wowapp.rpc.domain.ai;

import com.wowapp.rpc.domain.Bet;
import com.wowapp.rpc.domain.ai.strategy.MostFrequentPlayersBetPredictionStrategy;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * This class should be responsible for some statistical test magic based on prediction type
 * but for demo purpose I'll choose result of MostFrequentPlayersBetPredictionStrategy
 * Author Sergii Motynga.
 */
@Component
public class BetNegotiation {

    public Bet makeChoose(List<Prediction> predictions) {
        return predictions.stream()
                .filter(p -> p.getStrategy().equals(MostFrequentPlayersBetPredictionStrategy.class))
                .map(Prediction::getBet)
                .map(Optional::get)
                .findFirst()
                //because for demo we are using antagonist most frequent players choice
                //we have to have at least one round in history
                //it means that first prediction will be empty and we need some first bet
                .orElse(Bet.ROCK);

    }
}
