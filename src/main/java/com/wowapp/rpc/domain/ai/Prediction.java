package com.wowapp.rpc.domain.ai;

import com.wowapp.rpc.domain.Bet;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Optional;

/**
 * Author Sergii Motynga.
 */
@AllArgsConstructor(staticName = "of")
@Getter
public class Prediction {
    private final Optional<Bet> bet;
    private Class<? extends BetPredictionStrategy> strategy;
}
