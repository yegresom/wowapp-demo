package com.wowapp.rpc.domain.ai;

import com.wowapp.rpc.domain.Bet;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Service is responsible for collection all possible bets from all strategies and
 * use {@link BetNegotiation} to get best bet
 * Author Sergii Motynga.
 */
@Component
@AllArgsConstructor
public class PredictionService {

    private List<BetPredictionStrategy> strategies;

    private BetNegotiation negotiation;

    public Bet makeBet() {
        //computation could be parallel
        List<Prediction> predictions = strategies.stream()
                .map(strategy -> Prediction.of(strategy.prediction(), strategy.getClass()))
                .filter(prediction -> prediction.getBet().isPresent())
                .collect(toList());

        return negotiation.makeChoose(predictions);
    }
}
