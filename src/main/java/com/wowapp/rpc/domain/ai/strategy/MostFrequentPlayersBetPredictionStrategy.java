package com.wowapp.rpc.domain.ai.strategy;

import com.wowapp.rpc.domain.Bet;
import com.wowapp.rpc.domain.HistoryHolder;
import com.wowapp.rpc.domain.RoundResult;
import com.wowapp.rpc.domain.ai.BetPredictionStrategy;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

/**
 * Creates prediction base on most frequent players choice in history
 * <p/>
 * Author Sergii Motynga.
 */
@Component
@AllArgsConstructor
public class MostFrequentPlayersBetPredictionStrategy implements BetPredictionStrategy {

    private HistoryHolder historyHolder;

    @Override
    public Optional<Bet> prediction() {
        List<RoundResult> history = historyHolder.getAllHistory();
        if (history.isEmpty()) return Optional.empty();

        return history.stream().collect(groupingBy(RoundResult::getPlayersBet, counting()))
                .entrySet().stream()
                .max(comparing(Map.Entry::getValue))
                .map(Map.Entry::getKey)
                .map(Bet::antagonist);
    }
}
