package com.wowapp.rpc.domain.ai.strategy;

import com.wowapp.rpc.domain.Bet;
import com.wowapp.rpc.domain.HistoryHolder;
import com.wowapp.rpc.domain.RoundResult;
import com.wowapp.rpc.domain.ai.BetPredictionStrategy;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * This strategy returns {@link Bet} antagonist of previous AI choice
 * Author Sergii Motynga.
 */
@Component
@AllArgsConstructor
public class PreviousAIChoiceBetPredictionStrategy implements BetPredictionStrategy {

    private HistoryHolder historyHolder;

    @Override
    public Optional<Bet> prediction() {
        List<RoundResult> history = historyHolder.getAllHistory();
        if (history.isEmpty()) return Optional.empty();

        return Optional.of(Bet.antagonist(history.get(history.size() - 1).getAiBet()));
    }
}
