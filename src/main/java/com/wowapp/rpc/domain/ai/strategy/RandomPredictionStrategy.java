package com.wowapp.rpc.domain.ai.strategy;

import com.wowapp.rpc.domain.Bet;
import com.wowapp.rpc.domain.ai.BetPredictionStrategy;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Random;

/**
 * Just return random {@link Bet}
 * Author Sergii Motynga.
 */
@Component
public class RandomPredictionStrategy implements BetPredictionStrategy {

    private static final Random random = new Random();

    @Override
    public Optional<Bet> prediction() {
        return Optional.of(Bet.values()[random.nextInt(Bet.values().length)]);
    }
}
