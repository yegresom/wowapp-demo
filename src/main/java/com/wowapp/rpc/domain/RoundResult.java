package com.wowapp.rpc.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Immutable round result holder.
 * Author Sergii Motynga.
 */
@Getter
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode
public class RoundResult {
    private final Bet playersBet;
    private final Bet aiBet;

    @JsonProperty
    public String winner() {
        if (playersBet.equals(aiBet)) return "DRAW";
        return Bet.antagonist(aiBet).equals(playersBet) ? "PLAYER" : "AI";
    }
}
