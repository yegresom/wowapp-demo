package com.wowapp.rpc.web;

import com.wowapp.rpc.domain.Bet;
import com.wowapp.rpc.domain.HistoryHolder;
import com.wowapp.rpc.domain.RPCService;
import com.wowapp.rpc.domain.RoundResult;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Author Sergii Motynga.
 */
@RestController
@AllArgsConstructor
public class RPCController {

    private RPCService service;

    private HistoryHolder historyHolder;

    @RequestMapping("/round")
    public RoundResult round(@RequestParam("bet") Bet bet) {
        return service.process(bet);
    }

    @RequestMapping("history")
    public List<RoundResult> history() {
        return historyHolder.getAllHistory();
    }
}
